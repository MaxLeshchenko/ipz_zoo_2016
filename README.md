Create schema 'zoo', configure db properties in spring-core-config.xml
Run ua.nure.ipz.zoo.demo.Program to fill up database with test data.
Pack as a war and run as tomcat webapp (can be configured in Idea - Edit Configurations -> New Configuration -> Tomcat Server(local) (in Deployment tab you can add WAR from existing module))